#!/bin/bash

echo "Date : 11 december 2023"
echo "Workflow for RRBS data analysis from Stategra datasets."
echo "Inputs : paths to scripts for quality control, trimming, alignment and methylation analysis."
echo "Outputs : trimmed fastq files, qc files, bam (unsorted and sorted) files, bedgraph files,..."

# Handling errors
set -o errexit # ensure script will stop in case of ignored error

PROJECT_DIR="$HOME"/naell_lenoir_uca_m2bi_hpc

# First step : initial quality control
step1=$(sbatch --parsable "$PROJECT_DIR"/scripts/rrbs_qc-init.slurm)
echo "$step1 : Initial Quality Control"

# Second step : trimming
step2=$(sbatch --parsable --dependency=afterok:$step1 "$PROJECT_DIR"/scripts/rrbs_trimming.slurm)
echo "$step2 : Trimming with TrimGalore"

# Third step : post-trimming quality control
step3=$(sbatch --parsable --dependency=afterok:$step2 "$PROJECT_DIR"/scripts/rrbs_qc-post.slurm)
echo "$step3 : Post-Trimming Quality Control"

# Fourth step : bismark genome preparation
step4=$(sbatch --parsable --dependency=afterok:$step3 "$PROJECT_DIR"/scripts/genome-preparation.slurm)
echo "$step4 : Preparing Reference Genome - Bismark"

# Fifth step : bismark alignment
step5=$(sbatch --parsable --dependency=afterok:$step4 "$PROJECT_DIR"/scripts/rrbs_bismark-alignment.slurm)
echo "$step5 : Alignment"

# Sixth step : sorting BAM files
step6=$(sbatch --parsable --dependency=afterok:$step5 "$PROJECT_DIR"/scripts/rrbs_sorting-bam.slurm)
echo "$step6 : sorting BAM files"

#Seventh (and last) step : methylation analysis with rrbs_methylkit.R script
step7=$(sbatch --parsable --dependency=afterok:$step6 "$PROJECT_DIR"/scripts/rrbs_methylkit-analysis.slurm)
echo "$step7 : methylation analysis using methylKit"
