#!/bin/bash
#SBATCH --job-name=fastqc_pretrimming 
#SBATCH --array=0-23
#SBATCH -o log/slurmjob-%A-%a
#SBATCH --time=00:30:00
#SBATCH --nodes=1
#SBATCH --cpus-per-task=6
#SBATCH --partition=normal
#SBATCH --mem-per-cpu=2G

echo "Starting quality control." >&2

# Handling errors
set -o errexit # ensure script will stop in case of ignored error

# Modules (purge will only be done once - first step of analysis)
echo "Loading all packages needed..." >&2
module purge 
module load java/oracle-1.7.0_79 fastqc/0.11.7

echo "Setting up directories..." >&2

# Data type
DATA_TYPE="rrbs"

# Data directory
echo "Setting up directories..." >&2
PROJECT_DIR="$HOME"/naell_lenoir_uca_m2bi_hpc
DATA_DIR="$PROJECT_DIR"/"$DATA_TYPE"

# List all the files
echo "Listing fasta.gz sequence files..." >&2
files=($(ls "$DATA_DIR"/*.fastq.gz))
shortname=($(basename ${files[$SLURM_ARRAY_TASK_ID]} .fastq.gz))

# Output and temporary directories
OUTPUT="$PROJECT_DIR"/results/"$DATA_TYPE"/FastQC/"$shortname"
SCRATCHDIR=/storage/scratch/"$USER"/"$shortname"/fastqc_pretrimming
mkdir -p $OUTPUT
mkdir -p -m 700 "$SCRATCHDIR"

cd "$SCRATCHDIR"

# Initial quality control
echo "Running initial quality control with FastQC on $shortname..."`date` >&2
fastqc "${files[$SLURM_ARRAY_TASK_ID]}" -o "$SCRATCHDIR" -dir "$SCRATCHDIR"
mv "$SCRATCHDIR" "$OUTPUT"

# Cleaning temporary directory
rm -rf "$SCRATCHDIR"

echo "Initial quality control completed for $shortname :"`date` >&2

