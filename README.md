# naell_lenoir_uca_m2bi_hpc



## HPC Project

GitHub repository of HPC project 2023-2024.  
This project is based on the analysis of a RRBS data subset from "STATegra, a comprehensive multi-omics dataset of B-cell differentiation in mouse", David Gomez-Cabrero et al. 2019, https://doi.org/10.1038/s41597-019-0202-7.

## RRBS analysis

The scripts are located in the scripts/ directory.  
Please make sure you've loaded the required environment before starting the analysis (rrbs-env in data/). You'll also need to have access to the RRBS subset from STATegra and to the reference genome of Mus musculus (GRCm39/mm10). Paths towards these data are indicated in data/rrbs-data.  
If you use symbolic links, make sure that the rrbs directory is in the "git home" (naell_lenoir_uca_m2bi_hpc/rrbs) and that the reference genome is in data/reference_genome (naell_lenoir_uca_m2bi_hpc/data/reference_genome). Otherwise, feel free to change some directories in some scripts.  
Now, everything should be fine !  


## Directories
log/ and results/ directories are empty for now.  
Don't forget to provide STATegra RRBS data and the reference genome !  
```
.
|-- README.md
|-- log
|   ...
|-- results
|   ...
|-- scripts
|   |-- rrbs_qc-init.slurm
|   |-- rrbs_trimming.slurm
|   `-- ...
`-- data
    |-- rrbs-env
    `-- rrbs-data

```
